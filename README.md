## CHAT UDP SIMPLES

Este repositório contém um programa servidor e cliente, em que o servidor recebe mensagens e o cliente manda mensagens a um servidor. Para a comunicação do servidor e cliente é usado o Protocolo UDP.
---

### Considerações Gerais:

* Para sair do chat do e finalizar a conexão do cliente:
    * Utilize "sair"

Sobre os comandos que o Makefile permite:

* "make clean" = apaga os arquivos objeto e binário;
* "make" = compila o programa;
* "make doc" = gera a documentação do programa, em que será possivel visualizar acesssando a pasta doc e abrindo o arquivo index.html.

---

### Para o lado servidor:
* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos:  "./servidor".

---

### Para o lado cliente:
* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos:  "./cliente ip_servidor";
    * ip_servidor = IP do servidor que está aguardando as mensagens
* Observação: para que o lado cliente consiga enviar mensagens ao servidor, o servidor já deve estar executando e aguardando mensagens.



