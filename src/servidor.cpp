/**
* @file	    servidor.cpp
* @brief	Arquivo com a função principal do programa, que representa o lado servidor do chat
* @author   Pedro Emerick (p.emerick@live.com)
* @since    10/12/2018
* @date     14/12/2018
*/

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;
using std::to_string;

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <cstring>
using std::memset;

#include <unistd.h>
#include <cstdlib>
#include <ctime>

/**
* @brief    Função principal do programa que representa o lado servidor do chat
*/
int main (int argc, char *argv[]) 
{
    // Verifica se os argumentos passados ao programa são válidos
    if (argc != 1) {
        cerr << "--> Argumentos invalidos !" << endl;
        cerr << "Utilize: ./bin/servidor" << endl;

        exit(1);
    }
    
    // Porta usada para conexão
    int porta = 1500;
    char msg[1000];

    // 
    /* Cria socket
    * AF_INET = dominio de comunicação (protocolo ipv4)
    * SOCK_DGRAM = tipo de conexão (UDP)
    * 0 = valor do protocolo (IP)
    */
    int server = socket(AF_INET, SOCK_DGRAM, 0);

    if (server < 0) {
        cerr << "--> Erro ao criar socket do servidor !" << endl;
        exit (1);
    }

    // FAZ A LIGACAO DE TODOS OS ENDERECOS COM A PORTA
    struct sockaddr_in servAddr;
    servAddr.sin_family = AF_INET;                  // Protocolo IPv4
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);   // Converte Host para Network Long (INADDR_ANY é qualquer endereço de entrada)
    servAddr.sin_port = htons(porta);               // Converte Host para Network Short

    // Associar um endereço e porta ao socket
    int bind_st = bind(server, (struct sockaddr*) &servAddr, sizeof(servAddr));

    if (bind_st < 0) {
        cerr << "--> Erro ao estabelecer socket do servidor !" << endl;
        exit (1);
    }

    cout << "--> Aguardando por mensagens na porta UDP(" << porta << ")" << endl;

    // Estrutura para dados dos clientes
    struct sockaddr_in clienAddr;
    socklen_t clienSize;
    
    int status;
    while (1)
    {
        // Limpa o vetor para receber as mensagens
        memset (msg, 0x0, 1000);

        // Pega o tamanho do socket do cliente
        clienSize = sizeof(clienAddr);

        /*  RECEBE A MSG
        *   server = socket que recebe a msg
        *   msg = vetor que armazena a msg
        *   1000 = tamanho maximo da msg
        *   0 = flags, ou seja, nenhuma flag
        *   clienAddr = de onde vem a msg
        *   clienSize = tamanho dos endereços que vem a msg
        */ 
        status = recvfrom(server, msg, 1000, 0, (struct sockaddr*) &clienAddr, &clienSize);

        if (status < 0) {
            cout << "--> Erro ao receber mensagem !" << endl;
            continue;
        }

        cout << "< " << inet_ntoa(clienAddr.sin_addr) << ":UDP(" << ntohs(clienAddr.sin_port) << ") : " << msg << endl;
    }

    // Fecha o socket
    close(server);

    return 0;
}