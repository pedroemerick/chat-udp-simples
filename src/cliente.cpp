/**
* @file	    cliente.cpp
* @brief	Arquivo com a função principal do programa, que representa o lado cliente do chat
* @author   Pedro Emerick (p.emerick@live.com)
* @since    10/12/2018
* @date     14/12/2018
*/

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;
using std::to_string;

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <cstring>
using std::memset;
using std::memcpy;

#include <unistd.h>
#include <netdb.h>
#include <cstdlib>
#include <ctime>

/**
* @brief    Função principal do programa que representa o lado cliente do chat
*/
int main (int argc, char *argv[]) 
{
    // Verifica se os argumentos passados ao programa são válidos
    if (argc != 2) {
        cerr << "--> Argumentos invalidos !" << endl;
        cerr << "Utilize: ./bin/cliente ip_servidor" << endl;

        exit(1);
    }
    
    // Porta usada para o chat
    int porta = 1500;

    char *servidorIP = argv[1];
    
    // Obtem e endereco ip e pesquisa o nome no dns
    struct hostent* host = gethostbyname(servidorIP);

    if (host == NULL) {
        cout << "--> Host desconhecido !" << endl;
    }

    cout << "--> Enviando mensagens para " << host->h_name<< " (IP : " << inet_ntoa(*(struct in_addr *)host->h_addr_list[0]) << ")" << endl;

    // Estrutura referente ao servidor
    struct sockaddr_in servAddr;
    servAddr.sin_family = host->h_addrtype;
    memcpy((char *) &servAddr.sin_addr.s_addr, host->h_addr_list[0], host->h_length);
    servAddr.sin_port = htons(porta);
 
    /* Cria socket
    * AF_INET = dominio de comunicação (protocolo ipv4)
    * SOCK_DGRAM = tipo de conexão (UDP)
    * 0 = valor do protocolo (IP)
    */
    int cliente = socket(AF_INET, SOCK_DGRAM, 0);

    if (cliente < 0) {
        cerr << "--> Erro ao criar socket do cliente !" << endl;
        exit (1);
    }

    // FAZ A LIGACAO DE TODOS OS ENDERECOS COM A PORTA
    struct sockaddr_in cliAddr;
    cliAddr.sin_family = AF_INET;
    cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    cliAddr.sin_port = htons(0);

    // Associar um endereço e porta ao socket
    int rc = bind (cliente, (struct sockaddr*) &cliAddr, sizeof(cliAddr));

    if (rc < 0) {
        cerr << "--> Erro ao ligar a porta !" << endl;
        exit (1);
    }

    while (1)
    {
        string texto;

        // Recebe a msg via terminal
        cout << "> ";
        string temp;
        getline(cin, temp);

        /* ENVIA A MSG
        *   cliente = socket que envia a msg
        *   temp.c_str() = msg a ser enviada
        *   temp.length() + 1 = tamanho da msg
        *   0 = flags, ou seja, nenhuma flag
        *   servAddr = para onde vai a msg
        *   sizeof(servAddr) = tamanho dos endereços que vai a msg
        */
        rc = sendto (cliente, temp.c_str(), temp.length() + 1, 0, (sockaddr*) &servAddr, sizeof(servAddr));

        if (rc < 0) {
            cerr << "--> Erro ao enviar mensagem !" << endl;
            close(cliente);
            exit (1);
        }

        if (temp.compare("sair") == 0) {
            break;
        }
    }

    // Fecha o socket
    close(cliente);

    return 0;
}